﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class Association : ComparableObject
    {
        protected IComparable key;
        protected object value;

        public Association(IComparable key, object value)
        {
            this.key = key;
            this.value = value;
        }

        public Association(IComparable key) : this(key, null)
        { }

        public IComparable Key
        { get { return key; } }
    
        public object Value
        { get { return value; } }

        public override int CompareTo(object obj)
        {
            Association association = obj as Association;
            return key.CompareTo(association.key);
        }

        public override string ToString()
        {
            string result = "Association {" + key;
            if (value != null)
                result += ", " + value;
            return result + "}";
        }
    }
}
