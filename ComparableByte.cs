﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableByte : ComparableValue
    {
        public ComparableByte(byte b) : base(b)
        { }

        public static explicit operator byte(ComparableByte c)
        {
            return (byte)c.obj;
        }
    }
}
