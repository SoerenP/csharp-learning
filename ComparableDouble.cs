﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableDouble : ComparableValue
    {
        public ComparableDouble(double d) : base(d)
        { }

        public static explicit operator double(ComparableDouble c)
        {
            return (double)c.obj;
        }
    }
}
