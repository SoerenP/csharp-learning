﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableBool : ComparableValue
    {
        public ComparableBool(bool b) : base(b)
        { }
    
        public static explicit operator bool(ComparableBool b)
        {
            return (bool)b.obj;
        }
    }
}
