﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public interface Visitor
    {
        void Visit(object obj);
        bool IsDone { get; }
    }
}
