﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableInt32 : ComparableValue
    {
        public ComparableInt32(int i) : base(i)
        { }

        public static explicit operator int(ComparableInt32 c)
        {
            return (int)c.obj;
        }
    }
}
