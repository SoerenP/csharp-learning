﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Comparable
{
    public interface Container : IComparable, IEnumerable
    {
        int Count { get; }
        bool IsEmpty { get; }
        bool IsFull { get; }
        void Purge();
        void Accept(Visitor visitor);
    }
}
