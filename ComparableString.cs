﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableString : ComparableValue
    {
        public ComparableString(string s) : base(s)
        { }

        public static explicit operator string(ComparableString s)
        {
            return (string)s.obj;
        }
    }
}
