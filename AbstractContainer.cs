﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Comparable
{
    public abstract class AbstractContainer :
        ComparableObject, Container
    {
        protected int count;

        public virtual int Count
        { get {return count; } }

        public virtual bool IsEmpty
        { get { return Count == 0; } }

        public virtual bool IsFull
        { get { return false; } }

        public virtual void Purge()
        {
            this.count = 0;
        }

        public virtual IEnumerator GetEnumerator()
        {
            return (IEnumerator) GetEnumerator();
        }

        private class ToStringVisitor : AbstractVisitor
        {
            StringBuilder builder = new StringBuilder();
            private bool comma = false;

            public override void Visit(object obj)
            {
                if (comma)
                    builder.Append(",");
                builder.Append(obj);
                comma = true;
            }

            public override string ToString()
            {
                return builder.ToString();
            }
        };

        public virtual void Accept(Visitor visitor)
        {
            foreach(object i in this)
            {
                if (visitor.IsDone)
                    return;
                visitor.Visit(i);
            }
        }

        public override string ToString()
        {
            Visitor visitor = new ToStringVisitor();
            Accept(visitor);
            return GetType().FullName + " { " + visitor + "}";
        }
    
    
    }
}
