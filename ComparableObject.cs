﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public abstract class ComparableObject : IComparable
    {
        public abstract int CompareTo(Object obj);

        private int Compare(object obj)
        {
            if (GetType() == obj.GetType())
                return CompareTo(obj);
            else
                return GetType().FullName.CompareTo(obj.GetType().FullName);
        }

        public override bool Equals(object obj)
        {
            return Compare(obj) == 0;
        }   

        public static bool operator ==(ComparableObject c, object o)
        {
            if ((object)c == null || (object)o == null)
                return (object)c == (object)o;
            else
                return c.Compare(o) == 0;
        }

        public static bool operator !=(ComparableObject c, object o)
        {
            if ((object)c == null || (object)o == null)
                return (object)c != (object)o;
            else
                return c.Compare(o) != 0;
        }

        public static bool operator <(ComparableObject c, object o)
        {
            return c.Compare(o) < 0;
        }

        public static bool operator >(ComparableObject c, object o)
        {
            return c.Compare(o) > 0;
        }
    
        public static bool operator <=(ComparableObject c, object o)
        {
            return c.Compare(o) <= 0;
        }

        public static bool operator >=(ComparableObject c, object o)
        {
            return c.Compare(o) >= 0;
        }
    
        public static implicit operator ComparableObject(char c)
        {
            return new ComparableChar(c);
        }

        public static explicit operator char(ComparableObject c)
        {
            return (char)((ComparableObject)c);
        }

        public static implicit operator ComparableObject(int i)
        {
            return new ComparableInt32(i);
        }

        public static explicit operator int(ComparableObject c)
        {
            return (int)((ComparableInt32)c);
        }

        public static implicit operator ComparableObject(double d)
        {
            return new ComparableDouble(d);
        }

        public static explicit operator double(ComparableObject c)
        {
            return (double)((ComparableDouble)c);
        }

        public static implicit operator ComparableObject(string s)
        {
            return new ComparableString(s);
        }

        public static explicit operator string(ComparableObject c)
        {
            return (string)((ComparableObject)c);
        }

        public static implicit operator ComparableObject(bool b)
        {
            return new ComparableBool(b);
        }

        public static explicit operator bool(ComparableObject c)
        {
            return (bool)((ComparableObject)c);
        }

        public static implicit operator ComparableObject(byte b)
        {
            return new ComparableByte(b);
        }

        public static explicit operator byte(ComparableObject c)
        {
            return (byte)((ComparableObject)c);
        }

        public static implicit operator ComparableObject(short s)
        {
            return new ComparableShort(s);
        }

        public static explicit operator short(ComparableObject c)
        {
            return (short)((ComparableObject)c);
        }

        public static implicit operator ComparableObject(long l)
        {
            return new ComparableLong(l);
        }

        public static explicit operator long(ComparableObject c)
        {
            return (long)((ComparableObject)c);
        }

        public static implicit operator ComparableObject(float f)
        {
            return new ComparableFloat(f);
        }

        public static explicit operator float(ComparableObject c)
        {
            return (float)((ComparableObject)c);
        }
    }
}
