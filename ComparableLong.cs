﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableLong : ComparableValue
    {
        public ComparableLong(long l) : base(l)
        { }

        public static explicit operator long(ComparableLong c)
        {
            return (long)c.obj;
        }
    }
}
