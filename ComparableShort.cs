﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableShort : ComparableValue
    {
        public ComparableShort(short s) : base(s)
        { }

        public static explicit operator short(ComparableShort s)
        {
            return (short)s.obj;
        }
    }
}
