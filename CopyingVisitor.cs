﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class CopyingVisitor : AbstractVisitor
    {
        protected SearchableContainer target;

        public CopyingVisitor(SearchableContainer target)
        {
            this.target = target;
        }

        public override void Visit(object obj)
        {
            if (!target.IsFull)
                target.Insert((ComparableObject)obj);
        }
    }
}
