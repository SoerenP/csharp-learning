﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableChar : ComparableValue
    {
        public ComparableChar(char c) : base(c)
        { }

        public static explicit operator char(ComparableChar c)
        {
            return (char)c.obj;
        }
    }
}
