﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class CuttingVisitor : AbstractVisitor
    {
        protected SearchableContainer target;
        protected SearchableContainer source;

        public CuttingVisitor(SearchableContainer target, SearchableContainer source)
        {
            this.target = target;
            this.source = source;
        }

        public override void Visit(object obj)
        {
            if(!target.IsFull)
            {
                target.Insert((ComparableObject)obj);
                source.Withdraw((ComparableObject)obj);
            } 
        }

    }
}
