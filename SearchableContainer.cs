﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public interface SearchableContainer : Container
    {
        bool IsMember(ComparableObject c);
        void Insert(ComparableObject c);
        void Withdraw(ComparableObject c);
        ComparableObject Find(ComparableObject c);
    }
}
