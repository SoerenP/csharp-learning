﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public abstract class AbstractSearchableContainer : 
        AbstractContainer, SearchableContainer
    {
        public abstract void Insert(ComparableObject obj);
        public abstract void Withdraw(ComparableObject obj);

        public virtual bool IsMember(ComparableObject obj)
        {
            Visitor visitor = new MatchingVisitor((object)obj);
            Accept(visitor);
            return visitor.IsDone;
        }

        public virtual ComparableObject Find(ComparableObject obj)
        {
            Visitor visitor = new MatchingVisitor((object)obj);
            Accept(visitor);
            MatchingVisitor m_visitor = visitor as MatchingVisitor;
            return m_visitor.GetFound() as ComparableObject;
        }
    }
}
