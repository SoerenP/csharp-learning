﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public abstract class ComparableValue : ComparableObject
    {
        protected IComparable obj;

        public ComparableValue(IComparable obj)
        {
            this.obj = obj;
        }

        public virtual object Object { get { return obj; } }

        public override int CompareTo(object arg)
        {
            ComparableValue cv = arg as ComparableValue;
            if (obj.GetType() == cv.obj.GetType())
                return obj.CompareTo(cv.obj);
            else
                return obj.GetType().FullName.CompareTo(
                    cv.obj.GetType().FullName);
        }

        public override int GetHashCode()
        {
            return obj.GetHashCode();
        }

        public override string ToString()
        {
            return obj.ToString();
        }
    }
}
