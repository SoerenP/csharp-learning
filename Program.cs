﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Comparable;

namespace Hashing
{
    class Program
    {
        static void Main(string[] args)
        {
            int t = 102445;
            Console.WriteLine(SimpleHashing.DivisionHash(t));
            Console.WriteLine(SimpleHashing.MiddleSquareHash(t));
            Console.WriteLine(SimpleHashing.FibonacciHash32(t));

            ComparableObject i = 5;
            ComparableObject j = 7;
            bool result = i < j;
            Console.WriteLine(result.ToString());

            ComparableObject a = new Association(3, 4);
            ComparableObject b = new Association(3);
            result = (a == b);
            Console.WriteLine(result.ToString());
        }
    }

    public class SimpleHashing
    {
        private static int k = 10;
        private static int w = 32;

        // Takes mod with a prime. Consequtive values of input are hashed consequtively. Not good in practice
        public static int DivisionHash(int x)
        {
            int M = 1031; // a prime!
            return Math.Abs(x) % M;
        }
    
        public static int MiddleSquareHash(int x)
        {
            return (int)((uint)(x * x) >> (w - k));
        }
    
        public static int FibonacciHash32(Int32 x)
        {
            uint a = 2654435769U; // not many trailing or leading 0 or 1's. Relatively prime to 2^32. 
            return (int)((uint)(x * a) >> (w - k));
        }

        public static Int64 FibonacciHash64(Int64 x)
        {
            UInt64 a = 11400714819323198485U;
            return (Int64)((UInt64)(((UInt64)x) * a) >> (w - k));
        }

    
    }


}
