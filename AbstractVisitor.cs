﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public abstract class AbstractVisitor : Visitor
    {
        public virtual void Visit(object obj)
        { }

        public virtual bool IsDone
        { get {return false;} }
    }
}
