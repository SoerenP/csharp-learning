﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class ComparableFloat : ComparableValue
    {
        public ComparableFloat (float f) : base(f)
        { }

        public static explicit operator float(ComparableFloat c)
        {
            return (float)c.obj;
        }
    }
}
