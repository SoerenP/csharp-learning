﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Comparable
{
    public class MatchingVisitor : Visitor
    {
        private object target;
        private object found;

        public MatchingVisitor(object target)
        {
            this.target = target;
        }

        public void Visit(object obj)
        {
            if (!IsDone && obj.Equals(target))
                found = obj;
        }
        
        public bool IsDone
        {
            get { return found != null; }
        }

        public object GetFound()
        {
            return found;
        }
    }
}
